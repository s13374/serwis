<%@page import="com.example.servletjspdemo.domain.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>POKAZ ZAREJESTROWANYCH</title>
</head>
<body>

<jsp:useBean id="storage" class="com.example.servletjspdemo.service.StorageService" scope="application" />
<%
  for (User user : storage.getAllUsers()) {
	  out.println("<p>Username: " + user.getUsername() + "; Mail: " + user.getEmail()+ "</p>");
  }
%>
<p>
  <a href="index.jsp">GO HOME</a>
</p>

</body>
</html>