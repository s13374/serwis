package com.example.servletjspdemo.domain;

public class User {
	
	private String username;
	private String password;
        private String email;
        private boolean premium;
        
	
	public User() {
		super();
	}
	
	public User(String login, String pass, String mail) {
		super();
                
                this.username = login;
                this.password = pass;
                this.email = mail;
                this.premium = false;
	}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }


}
