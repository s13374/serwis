package com.example.servletjspdemo.web;

import com.example.servletjspdemo.domain.User;
import com.example.servletjspdemo.service.StorageService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sun.awt.AppContext;

@WebServlet(urlPatterns = {"/RegisterUser"})
public class RegisterUser extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        String recivedUsername = req.getParameter("username");
        String recivedPassword = req.getParameter("password");
        String recivedConffPassword = req.getParameter("confpass");
        String recivedEmail = req.getParameter("email");
        
        if (recivedPassword.equals(recivedConffPassword))
        {
            // zarejestrowac  
            User userToRegister = new User(recivedUsername, recivedPassword, recivedEmail);
            StorageService ourDatabase = (StorageService) getServletContext().getAttribute("storage");
            ourDatabase.add(userToRegister);
            
            resp.sendRedirect("showAllUsers.jsp");
            //PrintWriter out = resp.getWriter();
            //out.println("<html><body><p>ZAREJESTROWANO " + recivedUsername + "</p></body></html>");
        } 
        else
        {
            // wyswietlic blad 
            
         
            PrintWriter out = resp.getWriter();
            out.println("<html><body><p>BLAD " + recivedUsername + "</p></body></html>");
        }
    }
    
    
}
