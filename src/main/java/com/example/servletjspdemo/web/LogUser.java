package com.example.servletjspdemo.web;

import com.example.servletjspdemo.domain.User;
import com.example.servletjspdemo.service.StorageService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns = {"/LogUser"})
public class LogUser  extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       
        String recivedUsername = req.getParameter("username");
        String recivedPassword = req.getParameter("password");
    
        StorageService ourDatabase = (StorageService) getServletContext().getAttribute("storage");
        boolean canUserLogIn = ourDatabase.findUser(recivedUsername, recivedPassword);
        if(canUserLogIn == true){
             PrintWriter out = resp.getWriter();
            out.println("<html><body><p>SUKCES ZALOGOWANO UZYTKOWNIKA " + recivedUsername + "</p></body></html>");
            
                   
        
        }
        else {
            PrintWriter out = resp.getWriter();
            out.println("<html><body><p>BLAD NIE ZALOGOWANO " + recivedUsername + "</p></body></html>");
            
        
        
        }
    
    }

    
}
