package com.example.servletjspdemo.service;

import java.util.ArrayList;
import java.util.List;

import com.example.servletjspdemo.domain.User;

public class StorageService {
	
	private List<User> db = new ArrayList<User>();
	
	public void add(User user){
            User newPerson = new User(user.getUsername(), user.getPassword(), user.getEmail());
            db.add(newPerson);
	}
	
        public boolean findUser(String receivedUsername, String receivedPassword){
            for (User userFromDB : db){
                if(receivedUsername.equals(userFromDB.getUsername()) 
                   && receivedPassword.equals(userFromDB.getPassword()))
                {
                    return true;
                }
            }
            
            return false;
        }
        
	public List<User> getAllUsers(){
		return db;
	}

}
